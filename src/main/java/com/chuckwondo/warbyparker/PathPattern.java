package com.chuckwondo.warbyparker;

import java.util.regex.Pattern;

/**
 * <p>
 * A compiled representation of a pattern for matching paths.
 * </p>
 * 
 * <p>
 * A pattern is given by a comma-separated sequence of non-empty fields,
 * where each field consists of one or more characters, excluding the comma
 * character.
 * </p>
 * 
 * <p>
 * A path is given by a forward slash-separated sequence of non-empty
 * fields, where each field consists of one or more characters, excluding
 * the forward slash character.
 * </p>
 * 
 * <p>
 * In both cases, a field is considered empty if it has zero length after
 * trimming whitespace, but leading and trailing whitespace are left intact
 * for non-empty fields.
 * </p>
 * 
 * <p>
 * For a pattern to {@code #matches(String) match} a path, each field in
 * the pattern must exactly match the corresponding field in the path. For
 * example, the pattern {@code x,y} can only match the path {@code x/y}.
 * Note that leading and trailing slashes in paths are ignored, so that
 * {@code x/y} and {@code /x/y/} are equivalent.
 * </p>
 * 
 * <p>
 * Any field in a pattern may consist of a lone asterisk, which is a
 * wildcard, and can match any string within a corresponding path field.
 * For example, the pattern {@code A,*,B,*,C} consists of five fields:
 * three strings and two wildcards. It will successfully match the paths
 * {@code A/foo/B/bar/C} and {@code A/123/B/456/C}, but not {@code A/B/C},
 * {@code A/foo/bar/B/baz/C}, nor {@code foo/B/bar/C}.
 * </p>
 * 
 * <p>
 * Typical usage is as follows:
 * </p>
 * 
 * <blockquote>
 * 
 * <pre>
 * PathPattern p = new PathPattern(&quot;*,b,*&quot;);
 * boolean b = p.matches(&quot;/a/b/c&quot;);
 * </pre>
 * 
 * </blockquote>
 * 
 * @see PathMatcher
 */
public final class PathPattern implements Comparable<PathPattern> {
    /** Pattern string specified at creation time. */
    private final String pattern;

    /**
     * Ranking, relative to other patterns, based upon the number and
     * position of wildcards contained within this pattern.
     */
    private final double rank;

    /**
     * Pattern string (specified at creation time) translated into a
     * regular expression for pattern matching.
     */
    private final Pattern regex;

    /** Human-readable representation of this path pattern. */
    private String stringValue;

    /**
     * <p>
     * Creates a new {@code PathPattern} that represents the specified
     * pattern.
     * </p>
     * 
     * @param pattern
     *        path pattern to represent
     * @throws IllegalArgumentException
     *         if the specified pattern contains an empty field
     * @throws NullPointerException
     *         if the specified pattern is {@code null}
     */
    public PathPattern(String pattern) {
        // Split with negative value in second argument to prevent
        // discarding trailing empty strings. This allows us to catch
        // trailing empty fields in the pattern.
        String[] fields = pattern.split(",", -1);
        StringBuilder regexBuilder = new StringBuilder("/?");

        // See getRank(). Initialize to zero and add terms for wildcards.
        double rank = 0.0;
        // See getRank(). Initial term in geometric progression (a = 1/2)
        double term = 0.5;

        // Translates the specified pattern into an equivalent regular
        // expression, where each comma is translated into a slash, and
        // each wildcard (*) is translated into a regex that matches one or
        // more characters other than slashes, which is [^/]+, and each
        // non-wildcard field is quoted. The entire regex is then
        // surrounded by optional slashes to account for optional leading
        // and trailing slashes. For example, *,b,* translates to the
        // following regex: /?[^/]+/\Qb\E/[^/]+/?

        for (String field : fields) {
            if (field.trim().isEmpty()) {
                throw new IllegalArgumentException(
                    "Empty fields are not permitted: " + pattern);
            }

            if (field.equals("*")) {
                regexBuilder.append("[^/]+/");
                rank += 1 + term;
            } else {
                regexBuilder.append("\\Q");
                regexBuilder.append(field);
                regexBuilder.append("\\E/");
            }

            // Compute next term in geometric progression (r = 1/2)
            term /= 2;
        }

        regexBuilder.append("?");

        this.pattern = pattern;
        this.regex = Pattern.compile(regexBuilder.toString());
        this.rank = rank;
    }

    /**
     * <p>
     * This implementation compares this pattern to the specified pattern
     * based on ranks and string values, respectively. This pattern is
     * considered less than or greater than the specified object, if its
     * rank is less than or greater than the specified object,
     * respectively. In case of a tie, ordering is based upon the
     * lexicographical ordering of the patterns specified at the creation
     * times of the objects. If the pattern strings are equal, then this
     * object is considered equal to the specified object, which is
     * consistent with the implementation of {@link #equals(Object)}.
     * </p>
     * 
     * @see #getRank()
     * @see #equals(Object)
     */
    @Override
    public int compareTo(PathPattern other) {
        int rankSignum = (int) Math.signum(getRank() - other.getRank());

        return (rankSignum != 0) ? rankSignum : getPattern().compareTo(
            other.getPattern());
    }

    /**
     * <p>
     * Returns {@code true} if the specified object is this object, or it
     * is not {@code null}, is an instance of the same class, and has a
     * pattern string (specified at creation time) equal (case-sensitive)
     * to this instance's pattern string.
     * </p>
     */
    @Override
    public boolean equals(Object other) {
        return (other == this)
            || ((other != null) && (other.getClass() == getClass()) && ((PathPattern) other)
                .getPattern().equals(getPattern()));
    }

    /**
     * <p>
     * Returns the string value specified at creation time.
     * </p>
     * 
     * @return the string value specified at creation time
     */
    public String getPattern() {
        return this.pattern;
    }

    /**
     * <p>
     * Returns this pattern's rank, based on the number and position of
     * field wildcards (asterisks) it contains. Computed as the sum of the
     * number of wildcards and the terms in the geometric series given by
     * <em>a</em> = 1/2 and <em>r</em> = 1/2.
     * </p>
     * <p>
     * For example, the rank for the pattern {@code *,a,*} is computed as:
     * </p>
     * 
     * <pre>
     * 2 + 1/2 + 1/8 = 2.675
     * </pre>
     * <p>
     * The first number (2) is the number of wildcards, the second number
     * (1/2) is the first term of the geometric series (because there is a
     * wildcard in the first field), and the third number (1/8) is the
     * third number in the geometric series (because there is a wildcard in
     * the third field).
     * </p>
     * <p>
     * This guarantees that patterns are ranked first by the number of
     * wildcards, then by the positions of the wildcards. That is, all
     * patterns with {@code n} wildcards have a smaller rank than patterns
     * with {@code n + 1} wildcards, and in the case of a tie, the pattern
     * with the leftmost wildcard farthest to the right, will have a
     * smaller rank.
     * </p>
     * 
     * @return this pattern's rank, based on the number and position of
     *         wildcards it contains
     * @see <a
     *      href="http://en.wikipedia.org/wiki/Geometric_series">Geometric
     *      series</a>
     */
    public double getRank() {
        return this.rank;
    }

    /**
     * <p>
     * Returns the hash code of the string value specified at creation
     * time.
     * </p>
     * 
     * @return the hash code of the string value specified at creation time
     */
    @Override
    public int hashCode() {
        return this.pattern.hashCode();
    }

    /**
     * <p>
     * Returns {@code true} if the specified path matches this pattern;
     * {@code false} otherwise.
     * </p>
     * 
     * @param path
     *        path to match against this pattern
     * @return {@code true} if the specified path matches this pattern;
     *         {@code false} otherwise
     * @throws IllegalArgumentException
     *         if the specified path contains an empty field
     * @throws NullPointerException
     *         if the specified path is {@code null}
     */
    public boolean matches(String path) {
        String[] fields = path.split("/");

        // String.split will result in an empty first field if the path
        // contains a leading slash, so we'll start at field index 1, if
        // that's the case. However, if there is a trailing slash, the
        // trailing empty string is discarded automatically, so we don't
        // have to handle it ourselves.
        int i = path.startsWith("/") ? 1 : 0;

        for (int n = fields.length; i < n; i++) {
            if (fields[i].trim().isEmpty()) {
                throw new IllegalArgumentException(
                    "Empty fields are not permitted: " + path);
            }
        }

        return this.regex.matcher(path).matches();
    }

    /**
     * <p>
     * This implementation returns a string containing the pattern string
     * specified at creation time (given by {@link #getPattern()}) and this
     * pattern's rank (given by {@link #getRank()}).
     * </p>
     */
    @Override
    public String toString() {
        if (this.stringValue == null) {
            StringBuilder builder = new StringBuilder("{ ");

            builder.append("'");
            builder.append(this.pattern);
            builder.append("'");
            builder.append(", ");
            builder.append(this.rank);
            builder.append(" }");

            this.stringValue = builder.toString();
        }

        return this.stringValue;
    }
}
