package com.chuckwondo.warbyparker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.chuckwondo.warbyparker.PathMatcher.Builder;

/**
 * <p>
 * Drives usage of a {@link PathMatcher}.
 * </p>
 * 
 * <p>
 * Input is obtained from standard input, and is expected to be formatted
 * as follows: The first line contains an integer, N, specifying the number
 * of patterns. The following N lines contain one pattern per line. Every
 * pattern is assumed to be unique. The next line contains a second
 * integer, M, specifying the number of paths. The following M lines
 * contain one path per line. ASCII character encoding is expected.
 * </p>
 * 
 * <p>
 * For each path encountered in the input, prints the best-matching pattern
 * to standard output. Each line in the output will end with a Unix-style
 * newline character (including the final line). If no pattern matches the
 * path, prints {@code NO MATCH}.
 * </p>
 * 
 * @see PathPattern
 */
public class PathMatcherDriver {
    /**
     * <p>
     * Functions as described above.
     * </p>
     * 
     * @param args
     *        ignored
     * @throws NumberFormatException
     *         if the value of the pattern count or the path count cannot
     *         be converted to an integer
     * @throws NullPointerException
     *         if there are fewer input lines than expected, based upon the
     *         pattern and path counts
     * @throws IOException
     *         if an I/O error occurred
     */
    public static void main(String[] args) throws IOException {
        Builder b = new PathMatcher.Builder();
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in,
            "US-ASCII"));

        int patternCount = Integer.valueOf(r.readLine());

        // Read patterns
        for (int i = 0; i < patternCount; i++) {
            b.addPattern(r.readLine());
        }

        int pathCount = Integer.valueOf(r.readLine());
        PathMatcher m = b.build();

        // For each path read from input, output best match (or "NO MATCH")
        for (int i = 0; i < pathCount; i++) {
            String path = r.readLine();
            String pattern = m.matchFast(path);

            if (pattern == null) {
                System.out.printf("NO MATCH\n");
            } else {
                System.out.printf("%s\n", pattern);
            }
        }
    }
}
