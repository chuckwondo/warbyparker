package com.chuckwondo.warbyparker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * <p>
 * Given a set of patterns, finds the pattern that "best" matches a given
 * path. The best-matching pattern is the one that matches the path using
 * the fewest wildcards. If there is a tie (that is, if two or more
 * patterns with the same number of wildcards match a path), the pattern
 * where the leftmost wildcard appears in a field farther to the right
 * wins. If multiple patterns' leftmost wildcards appear in the same field
 * position, this rule applies recursively to the remainder of the
 * patterns.
 * </p>
 * 
 * <p>
 * For example, given the patterns {@code *,*,c} and {@code *,b,*}, and the
 * path {@code /a/b/c/}, the best-matching pattern is {@code *,b,*}.
 * </p>
 * 
 * <p>
 * The following example code shows basic usage:
 * </p>
 * 
 * <pre>
 * Builder b = new PathMatcher.Builder();
 * 
 * b.addPattern(&quot;*,*,c&quot;);
 * b.addPattern(&quot;*,b,*&quot;);
 * 
 * PathMatcher m = b.build();
 * String p = m.match(&quot;/a/b/c/&quot;);
 * </pre>
 * 
 * <p>
 * This implementation provides <em>log</em><sub>2</sub>(<em>n</em>) time
 * for adding patterns, and <em>n</em><sup>2</sup> time for finding
 * matches.
 * </p>
 * 
 * <p>
 * <strong>Note that instances of {@code Builder} are not
 * synchronized.</strong> If multiple threads access a builder
 * concurrently, it must be synchronized externally.
 * </p>
 * 
 * <p>
 * However, instances of {@code PathMatcher} are immutable, and thus
 * require no synchronization. Also, they are not affected by further
 * changes to their builders.
 * </p>
 * 
 * @see PathPattern
 */
public final class PathMatcher {
    /**
     * Set of patterns provided by the builder specified at creation,
     * sorted by their <em>natural ordering</em>.
     */
    private final List<PathPattern> pathPatterns;

    /**
     * <p>
     * Creates a new {@code PathMatcher} containing all of the specified
     * patterns.
     * </p>
     */
    PathMatcher(SortedSet<PathPattern> patterns) {
        // Create defensive copy
        this.pathPatterns = Collections
            .unmodifiableList(new ArrayList<PathPattern>(patterns));
    }

    /**
     * <p>
     * Returns the pattern that best matches (as defined
     * {@link PathMatcher above}) the specified path.
     * </p>
     * 
     * @param path
     *        slash-separated path to match against the patterns added to
     *        this matcher
     * @return the pattern that best matches the specified path, or
     *         {@code null} if either there is no matching pattern or the
     *         specified path is {@code null}
     * @throws IllegalArgumentException
     *         if the specified path does not adhere to the defined format
     * @throws NullArgumentException
     *         if the specified path is {@code null}
     */
    public String match(String path) {
        for (PathPattern p : this.pathPatterns) {
            if (p.matches(path)) {
                return p.getPattern();
            }
        }

        return null;
    }

    /**
     * <p>
     * Returns the pattern that best matches (as defined
     * {@link PathMatcher above}) the specified path.
     * </p>
     * 
     * @param path
     *        slash-separated path to match against the patterns added to
     *        this matcher
     * @return the pattern that best matches the specified path, or
     *         {@code null} if either there is no matching pattern or the
     *         specified path is {@code null}
     * @throws IllegalArgumentException
     *         if the specified path does not adhere to the defined format
     * @throws NullArgumentException
     *         if the specified path is {@code null}
     */
    public String matchFast(String path) {
        for (PathPattern p : PathPatterns.matching(path)) {
            int index = Collections.binarySearch(this.pathPatterns, p);

            if (index >= 0) {
                return this.pathPatterns.get(index).getPattern();
            }
        }

        return null;
    }

    /**
     * <p>
     * Builds a {@code PathMatcher}.
     * </p>
     */
    public static class Builder {
        /** Set of patterns added to this builder. */
        private final SortedSet<PathPattern> pathPatterns = new TreeSet<PathPattern>();

        /**
         * <p>
         * Creates a new, empty {@code Builder}.
         * </p>
         */
        public Builder() {
            super();
        }

        /**
         * <p>
         * Adds the specified pattern to this builder's list of patterns.
         * </p>
         * 
         * @param pattern
         *        comma-separated pattern to add to this builder's list of
         *        patterns
         * @return {@code true} if this builder's pattern list was modified
         *         (that is, the specified pattern was not already added to
         *         this builder); {@code false} otherwise
         * @throws IllegalArgumentException
         *         if the specified pattern does not adhere to the defined
         *         format
         * @throws NullArgumentException
         *         if the specified pattern is {@code null}
         */
        public boolean addPattern(String pattern) {
            return this.pathPatterns.add(new PathPattern(pattern));
        }

        /**
         * <p>
         * Returns a {@code PathMatcher} that can match paths against the
         * patterns added to this builder, up to the point of invocation of
         * this method.
         * </p>
         * 
         * @return a {@code PathMatcher} that can match paths against the
         *         patterns added to this builder, up to the point of
         *         invocation of this method
         */
        public PathMatcher build() {
            // Pass pattern set to constructor, rather than the standard
            // practice of passing 'this', in order to avoid the following:
            // (1) increasing scope of pathPatterns member to default
            // (package private), or (2) having synthetic accessor
            // generated by the compiler by keeping fully private scope.
            return new PathMatcher(this.pathPatterns);
        }
    }
}
