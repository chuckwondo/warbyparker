package com.chuckwondo.warbyparker;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * Generates all {@link #matching matching} path patterns for a given path.
 * </p>
 * 
 * @see PathPattern
 */
public final class PathPatterns {
    /**
     * <p>
     * Prevent instantiation of this utility class.
     * </p>
     */
    private PathPatterns() {
        throw new UnsupportedOperationException();
    }

    /**
     * <p>
     * Generates all {@link #matching matching} path patterns for a given
     * path.
     * </p>
     * 
     * @param path
     * @return
     */
    public static Iterable<PathPattern> matching(String path) {
        return new PathPatternIterable(path);
    }

    /**
     * 
     */
    private static final class PathPatternIterable implements
        Iterable<PathPattern> {

        private final String path;

        PathPatternIterable(String path) {
            this.path = path;
        }

        @Override
        public Iterator<PathPattern> iterator() {
            return new PathPatternIterator(this.path);
        }
    }

    /**
     * 
     */
    private static final class PathPatternIterator implements
        Iterator<PathPattern> {

        /** */
        private final String[] fields;

        /** */
        private final List<Integer> indices;

        /** */
        private Iterator<List<Integer>> combos;

        /** */
        private int r;

        /**
         * 
         * @param path
         */
        PathPatternIterator(String path) {
            String[] fields = path.startsWith("/") ? path.substring(1).split(
                "/") : path.split("/");
            int r = fields.length;
            List<Integer> indices = new ArrayList<Integer>(r);

            for (int i = 0; i < fields.length; i++) {
                indices.add(i);
            }

            this.combos = new Combinator<Integer>(indices, r).iterator();
            this.fields = fields;
            this.indices = indices;
            this.r = r;
        }

        /**
         * 
         */
        @Override
        public boolean hasNext() {
            if (!this.combos.hasNext() && (this.r-- > 0)) {
                this.combos = new Combinator<Integer>(this.indices, this.r)
                    .iterator();
            }

            return this.combos.hasNext();
        }

        /**
         * 
         */
        @Override
        public PathPattern next() {
            List<Integer> indexes = this.combos.next();
            StringBuilder pattern = new StringBuilder();
            int r = indexes.size();

            for (int i = 0, j = 0; i < this.fields.length; i++) {
                if ((j < r) && (indexes.get(j) == i)) {
                    pattern.append(this.fields[i]);
                    j++;
                } else {
                    pattern.append("*");
                }

                pattern.append(",");
            }

            // Remove trailing comma
            pattern.setLength(pattern.length() - 1);

            return new PathPattern(pattern.toString());
        }

        /**
         * 
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
