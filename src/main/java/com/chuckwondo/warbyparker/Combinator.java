package com.chuckwondo.warbyparker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * Produces all combinations of a list of {@code n} elements taken
 * {@code r} at a time, where {@code 0 <= r <= n}.
 * </p>
 * 
 * @param <E>
 *        type of each element in the list
 */
public final class Combinator<E> implements Iterable<List<E>> {
    /** */
    private final Iterator<List<E>> comboIterator;

    /**
     * <p>
     * Creates a new {@code Combinator} that enables {@link #iterator()
     * iteration} over every combination of the specified elements taken
     * {@code r} at a time, where {@code 0 <= r <= n} and {@code n} is the
     * number of elements in the specified list.
     * </p>
     * 
     * @param elements
     *        elements to create combinations from
     * @param r
     *        number of elements to choose in each combination
     * @throws NullPointerException
     *         if the specified list of elements is {@code null}
     * @throws IllegalArgumentException
     *         if {@code r} is not in the range {@code [0, n]}, where
     *         {@code n} is the size of the specified list of elements to
     *         choose from
     */
    public Combinator(List<E> elements, int r) {
        if ((r < 0) || (elements.size() < r)) {
            throw new IllegalArgumentException(String.format(
                "Value of r (%d) is not in the range [0, %d]", r, elements
                    .size()));
        }

        // TODO Refactor to lazily create each combination during iteration

        // List of all combinations of r elements
        List<List<E>> combos = new ArrayList<List<E>>();

        if (r == 0) {
            // Selecting zero elements yields the empty list
            combos.add(Collections.<E> emptyList());
        } else {
            E head = elements.get(0);
            List<E> tail = elements.subList(1, elements.size());

            // Find all combinations of (r - 1) elements of the tail of the
            // list and prefix each one with the head of the list.
            for (List<E> tailCombo : new Combinator<E>(tail, r - 1)) {
                List<E> combo = new ArrayList<E>(r);

                combo.add(head);
                combo.addAll(tailCombo);

                // Make the combination unmodifiable, then add it to the
                // list of combinations.
                combos.add(Collections.unmodifiableList(combo));
            }

            // Find all combinations of r elements of the tail of the list.
            if (r <= tail.size()) {
                for (List<E> combo : new Combinator<E>(tail, r)) {
                    combos.add(combo);
                }
            }
        }

        // Make final list unmodifiable so that it cannot be modified
        // via the iterator's remove() method.
        this.comboIterator = Collections.unmodifiableList(combos).iterator();
    }

    /**
     * <p>
     * Returns an iterator that iterates over all combinations of the
     * elements specified at creation time of this {@code Combinator},
     * taken {@code r} at a time (also specified at creation time). Each
     * item returned by the iterator is a list of size {@code r} consisting
     * of elements from the list specified at creation time. Of course, if
     * {@code r} is zero, the iterator will produce a single item, which is
     * the empty list.
     * </p>
     * 
     * @return an iterator that iterates over all combinations of the
     *         elements specified at creation time of this
     *         {@code Combinator}, taken {@code r} at a time
     */
    @Override
    public Iterator<List<E>> iterator() {
        return this.comboIterator;
    }
}
