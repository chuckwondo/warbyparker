package com.chuckwondo.warbyparker;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PathPatternsTest {
    @Test
    public void matching() {
        List<PathPattern> expectedPatterns = new ArrayList<PathPattern>();

        expectedPatterns.add(new PathPattern("a,b,c"));
        expectedPatterns.add(new PathPattern("a,b,*"));
        expectedPatterns.add(new PathPattern("a,*,c"));
        expectedPatterns.add(new PathPattern("*,b,c"));
        expectedPatterns.add(new PathPattern("a,*,*"));
        expectedPatterns.add(new PathPattern("*,b,*"));
        expectedPatterns.add(new PathPattern("*,*,c"));
        expectedPatterns.add(new PathPattern("*,*,*"));

        int i = 0;

        for (PathPattern actualPattern : PathPatterns.matching("/a/b/c/")) {
            Assert.assertEquals(actualPattern, expectedPatterns.get(i++));
        }

        Assert.assertEquals(i, expectedPatterns.size());
    }
}
