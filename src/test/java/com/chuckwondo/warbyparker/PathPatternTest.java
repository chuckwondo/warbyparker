package com.chuckwondo.warbyparker;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PathPatternTest {
    @SuppressWarnings("unused")
    @Test(expectedExceptions = { NullPointerException.class })
    public void newPatternNull() {
        new PathPattern(null);
    }

    @SuppressWarnings("unused")
    @Test(expectedExceptions = { IllegalArgumentException.class })
    public void newPatternEmptyLeadingField() {
        new PathPattern(",b,c");
    }

    @SuppressWarnings("unused")
    @Test(expectedExceptions = { IllegalArgumentException.class })
    public void newPatternEmptyTrailingField() {
        new PathPattern("a,b,");
    }

    @Test
    public void compareEqualPatterns() {
        PathPattern p1 = new PathPattern("a,b,c");
        PathPattern p2 = new PathPattern("a,b,c");

        Assert.assertTrue(p1.compareTo(p2) == 0);
    }

    @Test
    public void compareWildcardsDifferentCount() {
        PathPattern p1 = new PathPattern("*,b,*");
        PathPattern p2 = new PathPattern("a,*,c");

        Assert.assertTrue(p1.compareTo(p2) > 0);
    }

    @Test
    public void compareWildcardsSameCountDifferentPositions() {
        PathPattern p1 = new PathPattern("*,b,*");
        PathPattern p2 = new PathPattern("a,*,*");

        Assert.assertTrue(p1.compareTo(p2) > 0);
    }

    @Test
    public void rankWithoutWildcards() {
        Assert.assertEquals(new PathPattern("a,b,c").getRank(), 0.0d);
    }

    @Test
    public void rankWithWildcards() {
        Assert.assertEquals(new PathPattern("*,b,*").getRank(), 2.625d);
    }

    @Test
    public void matchesLeadingSlash() {
        Assert.assertTrue(new PathPattern("a,b,c").matches("/a/b/c"));
    }

    @Test
    public void matchesTrailingSlash() {
        Assert.assertTrue(new PathPattern("a,b,c").matches("a/b/c/"));
    }

    @Test
    public void matchesWildcards() {
        Assert.assertTrue(new PathPattern("a,*,c").matches("a/foo/c"));
    }

    @Test
    public void matchesNoMatch() {
        Assert.assertFalse(new PathPattern("a,*,c").matches("a/foo/bar/c"));
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void matchesNull() {
        new PathPattern("a,b,c").matches(null);
    }

    @Test(expectedExceptions = { IllegalArgumentException.class })
    public void matchesEmptyField() {
        new PathPattern("a,b,c").matches("a//c");
    }
}
