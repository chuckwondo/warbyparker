package com.chuckwondo.warbyparker;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.chuckwondo.warbyparker.PathMatcher.Builder;

public class PathMatcherTest {
    @Test
    public void match() {
        Builder b = new PathMatcher.Builder();

        b.addPattern("*,b,c");
        b.addPattern("a,*,c");
        b.addPattern("a,b,*");

        Assert.assertEquals(b.build().match("/a/b/c"), "a,b,*");
    }

    @Test
    public void noMatch() {
        Builder b = new PathMatcher.Builder();

        b.addPattern("a,b,*");
        b.addPattern("*,b,c");

        Assert.assertNull(b.build().match("a/b/c/d"));
    }
}
