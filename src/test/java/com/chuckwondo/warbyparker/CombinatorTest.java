package com.chuckwondo.warbyparker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CombinatorTest {
    @SuppressWarnings("unused")
    @Test(expectedExceptions = { IllegalArgumentException.class })
    public void oneTakeTwo() {
        new Combinator<String>(Arrays.asList("a"), 2);
    }

    @SuppressWarnings("unused")
    @Test(expectedExceptions = { NullPointerException.class })
    public void nullTakeTwo() {
        new Combinator<String>(null, 2);
    }

    @Test
    public void oneTakeZero() {
        Iterator<List<String>> iterator = new Combinator<String>(Arrays
            .asList("a"), 0).iterator();

        Assert.assertTrue(iterator.hasNext(), "Result is empty");
        Assert.assertTrue(iterator.next().isEmpty(),
            "Combination is not empty.");
        Assert.assertFalse(iterator.hasNext(), "Result size is larger than 1");
    }

    @Test
    public void zeroTakeZero() {
        Iterator<List<String>> iterator = new Combinator<String>(Collections
            .<String> emptyList(), 0).iterator();

        Assert.assertTrue(iterator.hasNext(), "Result is empty");
        Assert.assertTrue(iterator.next().isEmpty(),
            "Combination is not empty.");
        Assert.assertFalse(iterator.hasNext(), "Result size is larger than 1");
    }

    @Test
    public void fiveTakeThree() {
        List<Integer> ints = Arrays.asList(0, 1, 2, 3, 4);
        Combinator<Integer> actualCombos = new Combinator<Integer>(ints, 3);
        List<List<Integer>> expectedCombos = new ArrayList<List<Integer>>(10);

        expectedCombos.add(Arrays.asList(0, 1, 2));
        expectedCombos.add(Arrays.asList(0, 1, 3));
        expectedCombos.add(Arrays.asList(0, 1, 4));
        expectedCombos.add(Arrays.asList(0, 2, 3));
        expectedCombos.add(Arrays.asList(0, 2, 4));
        expectedCombos.add(Arrays.asList(0, 3, 4));
        expectedCombos.add(Arrays.asList(1, 2, 3));
        expectedCombos.add(Arrays.asList(1, 2, 4));
        expectedCombos.add(Arrays.asList(1, 3, 4));
        expectedCombos.add(Arrays.asList(2, 3, 4));

        int i = 0;

        for (List<Integer> actualCombo : actualCombos) {
            Assert.assertEquals(actualCombo, expectedCombos.get(i++));
        }
    }
}
