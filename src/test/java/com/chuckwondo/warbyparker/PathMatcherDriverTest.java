package com.chuckwondo.warbyparker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PathMatcherDriverTest {
    @Test
    public void main() throws Throwable {
        InputStream sysin = System.in;
        PrintStream sysout = System.out;

        // Get test input stream from test input file on classpath
        InputStream in = getClass().getResourceAsStream("/test-input.txt");

        // Capture output in memory so we can compare actual output to
        // expected output.
        StringOutputStream sos = new StringOutputStream();
        PrintStream out = new PrintStream(sos);

        // Redirect I/O
        System.setIn(in);
        System.setOut(out);

        try {
            PathMatcherDriver.main(null);
        } finally {
            // Reset I/O
            System.setIn(sysin);
            System.setOut(sysout);

            in.close();
        }

        // Read expected output from file on classpath.
        BufferedReader r = new BufferedReader(new InputStreamReader(getClass()
            .getResourceAsStream("/test-output.txt")));
        String line = r.readLine();
        StringBuilder expectedOutput = new StringBuilder();

        while (line != null) {
            expectedOutput.append(line);
            expectedOutput.append('\n');
            line = r.readLine();
        }

        r.close();

        Assert.assertEquals(sos.getString(), expectedOutput.toString());
    }

    private static class StringOutputStream extends OutputStream {
        private final StringBuilder builder = new StringBuilder();

        public StringOutputStream() {
            super();
        }

        @Override
        public void write(int b) throws IOException {
            this.builder.append((char) b);
        }

        public String getString() {
            return this.builder.toString();
        }
    }
}
